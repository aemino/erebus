import 'dart:async';

import 'package:discidium/discord.dart';

import 'command/handler.dart';
import 'command/registry.dart';
import 'service/registry.dart';

class FrameworkClientOptions extends ClientOptions {
  final List<String> prefixes;

  FrameworkClientOptions({this.prefixes});
}

class FrameworkClient extends Client {
  // ignore: overridden_fields
  final FrameworkClientOptions options;

  CommandRegistry commands;
  ServiceRegistry services;

  CommandHandler commandHandler;

  FrameworkClient(String token, this.options) : super(token, options) {
    commands = CommandRegistry(this);
    services = ServiceRegistry(this);

    commandHandler = CommandHandler(this);

    onReady.first.then((event) => setup());
  }

  Future<void> setup() async {
    onMessageCreate.listen(commandHandler.handleMessage);

    await services.setupAll();
  }
}
