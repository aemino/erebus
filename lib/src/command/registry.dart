import '../client.dart';
import '../common/registry.dart';
import 'structure.dart';

class _CommandRegistryNode {
  final Command command;
  final Map<String, _CommandRegistryNode> children = {};

  _CommandRegistryNode(this.command);
}

class CommandRegistry extends Registry<Command> {
  final _rootNode = _CommandRegistryNode(null);

  CommandRegistry(FrameworkClient client) : super(client);

  void registerGroup(CommandGroup group) => group.commands.forEach(register);

  void register(Command command) => _register(command, _rootNode);

  void _register(Command command, _CommandRegistryNode node) {
    final commandNode = _CommandRegistryNode(command);
    command.names.forEach((name) => node.children[name] = commandNode);
    command.subcommands
        .forEach((subcommand) => _register(subcommand, commandNode));
  }

  Command get(String identifier) => identifier
      .split(' ')
      .fold(_rootNode, (node, name) => node.commands[name])
      .command;

  _CommandRegistryNode getRootNode() => _rootNode;
}
