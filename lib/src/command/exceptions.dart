abstract class CommandException implements Exception {
  String toString();
}

abstract class LocalizedCommandException extends CommandException {
  final String messageKey;
  final Map<String, dynamic> messageContext;

  LocalizedCommandException(this.messageKey, [this.messageContext]);
}

class UserInputException extends LocalizedCommandException {
  UserInputException(String messageKey, [Map<String, dynamic> messageContext])
      : super(messageKey, messageContext);
}

enum ParsingError {
  unterminatedQuote,
  duplicateArgumentName,
  incompleteNamedArgument,
  unknownArgumentName,
  missingRequiredArgument
}

class ArgumentParsingException extends CommandException {
  final ParsingError error;
  final String argName;

  ArgumentParsingException(this.error, [this.argName]);
}
