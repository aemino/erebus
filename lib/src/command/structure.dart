import 'dart:async';

import 'package:discidium/discord.dart';

import '../client.dart';
import '../resolvers/base.dart';
import 'arguments.dart';
import 'handler.dart';

abstract class CommandGroup {
  String get name;

  List<Command> get commands;
}

abstract class Command {
  String get name => names.first;

  List<String> get names;
  List<Argument> get args;

  List<Command> get subcommands => [];

  ArgumentSchema argumentSchema;

  Command() {
    argumentSchema = ArgumentSchema(args);

    if (subcommands
        .any((subcommand) => subcommand.runtimeType == runtimeType)) {
      throw ArgumentError(
          'Command $name declares itself as a subcommand. Please ensure that '
          '$name is not extending a command which declares $name as one of its '
          'own subcommands.');
    }
  }

  FutureOr<void> execute(
      CommandContext context, Map<String, ContextWrappedArgument> args);
}

class CommandContext {
  final FrameworkClient client;
  final Message message;
  final ParsedMessage parsedMessage;

  CommandContext(this.client, this.message, this.parsedMessage);

  Map<String, ContextWrappedArgument> args(Map<String, ParsedArgument> args) =>
      args.map((k, v) => MapEntry(k, ContextWrappedArgument(this, v)));

  BuiltMessage respond(MessageBuilder builder(MessageBuilder message)) =>
      message.channel.compose(builder);
}

class ContextWrappedArgument {
  final CommandContext _context;
  final ParsedArgument _arg;

  ContextWrappedArgument(this._context, this._arg);

  FutureOr<T> resolve<T>(ResolverOptions<T> options) =>
      options.resolver.resolve(_context, _arg, options);
}
