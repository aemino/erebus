import 'dart:async';

import 'package:discidium/discord.dart';

import '../client.dart';
import 'arguments.dart';
import 'structure.dart';

class ParsedMessage {
  final Message message;
  final bool isCommand;
  final Command command;
  final List<String> arguments;

  ParsedMessage({this.message, this.isCommand, this.command, this.arguments});
}

class CommandHandler {
  final FrameworkClient client;

  CommandHandler(this.client);

  ParsedMessage parseMessage(Message message) {
    final content = message.content;

    final prefix = client.options.prefixes
        .firstWhere(content.startsWith, orElse: () => null);

    if (prefix == null)
      return ParsedMessage(message: message, isCommand: false);

    final parts = content.substring(prefix.length).split(' ');

    var commandNode = client.commands.getRootNode();
    final commandParts = parts.takeWhile((part) {
      final nextNode = commandNode.children[part];

      if (nextNode != null) commandNode = nextNode;
      return nextNode != null;
    }).toList();

    if (commandNode.command == null)
      return ParsedMessage(message: message, isCommand: false);

    final arguments = parts.sublist(commandParts.length);

    return ParsedMessage(
        message: message,
        isCommand: true,
        command: commandNode.command,
        arguments: arguments);
  }

  Future<void> executeCommand(ParsedMessage parsedMessage) async {
    final command = parsedMessage.command;

    try {
      final args =
          ArgumentParser.parse(parsedMessage.arguments, command.argumentSchema);
      final context =
          CommandContext(client, parsedMessage.message, parsedMessage);

      await command.execute(context, context.args(args));
    } catch (err) {
      print(err.toString());
    }
  }

  Future<void> handleMessage(MessageCreateEvent event) async {
    final message = event.message;

    if (message.channel is GuildChannelSnowflake) {
      final member = await message.member.fetch();
      final permissions =
          await member.permissionsIn(message.channel as GuildChannelSnowflake);

      if (!(permissions >= Permissions.sendMessages)) return;
    }

    final parsedMessage = parseMessage(message);
    if (!parsedMessage.isCommand) return;

    await executeCommand(parsedMessage);
  }
}
