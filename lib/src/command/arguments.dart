import 'exceptions.dart';

class Argument {
  final String name;
  final bool optional;

  const Argument(this.name, {this.optional = false});
}

class ArgumentSchema {
  final Map<String, Argument> args;
  final List<Argument> orderedArgs;
  final List<Argument> requiredArgs;
  final List<Argument> optionalArgs;

  factory ArgumentSchema(List<Argument> args) {
    final mapped = <String, Argument>{};
    final required = <Argument>[];
    final optional = <Argument>[];

    for (final arg in args) {
      if (mapped.containsKey(arg.name))
        throw ArgumentError(
            'Argument schema contains duplicate argument name: ${arg.name}.');

      mapped[arg.name] = arg;

      if (arg.optional) {
        optional.add(arg);
      } else {
        required.add(arg);
      }
    }

    return ArgumentSchema._internal(mapped, args, required, optional);
  }

  ArgumentSchema._internal(
      this.args, this.orderedArgs, this.requiredArgs, this.optionalArgs);
}

class ParsedArgument {
  final Argument _arg;
  final String _content;

  ParsedArgument(this._arg, this._content);

  ParsedArgument _extend(String content) =>
      ParsedArgument(_arg, _content + ' ' + content);

  String toString() => _content;
}

class ArgumentParser {
  static Map<String, ParsedArgument> parse(
      List<String> args, ArgumentSchema schema) {
    // Stage one:
    // - Parse quoted arguments
    // - Parse
    final namedArguments = <String, String>{};
    final unnamedArguments = <String>[];

    var argIndex = 0;
    String argName;

    void submitArg(String arg) {
      if (argName != null) {
        if (namedArguments.containsKey(argName))
          throw ArgumentParsingException(ParsingError.duplicateArgumentName);

        namedArguments[argName] = arg;
        argName = null;
      } else {
        unnamedArguments.add(arg);
      }
    }

    while (argIndex < args.length) {
      final arg = args[argIndex];

      if (arg.startsWith('"')) {
        final endIndex =
            args.sublist(argIndex).indexWhere((arg) => arg.endsWith('"')) +
                argIndex;

        if (endIndex < argIndex)
          throw ArgumentParsingException(ParsingError.unterminatedQuote);

        final content = args.sublist(argIndex, endIndex + 1).join(' ');

        submitArg(content.substring(1, content.length - 1));
        argIndex = endIndex + 1;

        continue;
      }

      if (arg.endsWith(':')) {
        if (argName != null)
          throw ArgumentParsingException(ParsingError.incompleteNamedArgument);

        argName = arg.substring(0, arg.length - 1);
        argIndex++;
        continue;
      }

      submitArg(arg);
      argIndex++;
    }

    if (argName != null)
      throw ArgumentParsingException(ParsingError.incompleteNamedArgument);

    // Stage two:
    // - Assign named and unnamed arguments to the argument schema

    final parsedArguments = <String, ParsedArgument>{};

    for (final entry in namedArguments.entries) {
      final arg = schema.args[entry.key];

      if (arg == null)
        throw ArgumentParsingException(
            ParsingError.unknownArgumentName, entry.key);

      parsedArguments[entry.key] = ParsedArgument(arg, entry.value);
    }

    final numRequiredArgs = schema.requiredArgs
        .where((arg) => !parsedArguments.containsKey(arg.name))
        .length;

    if (unnamedArguments.length < numRequiredArgs)
      throw ArgumentParsingException(ParsingError.missingRequiredArgument,
          schema.requiredArgs[numRequiredArgs - 1].name);

    var optionalArgQuota = unnamedArguments.length - numRequiredArgs;

    ParsedArgument lastArg;
    void consume(Argument arg) => lastArg = parsedArguments[arg.name] =
        ParsedArgument(arg, unnamedArguments.removeAt(0));

    for (final arg in schema.orderedArgs) {
      if (parsedArguments.containsKey(arg.name)) continue;

      if (arg.optional) {
        if (optionalArgQuota-- > 0) consume(arg);
        continue;
      }

      consume(arg);
    }

    // Insert any unused unnamed arguments into the final consumed argument.
    if (lastArg != null) {
      parsedArguments[lastArg._arg.name] =
          lastArg._extend(unnamedArguments.join(' '));
    }

    unnamedArguments.clear();

    return parsedArguments;
  }
}
