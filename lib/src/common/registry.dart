import '../client.dart';

abstract class Registry<T> {
  final FrameworkClient client;

  Registry(this.client);

  T get(String identifier);
  void register(T item);
}
