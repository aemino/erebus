import 'dart:async';

import '../client.dart';
import '../common/registry.dart';
import 'structure.dart';

class ServiceRegistry extends Registry<Service> {
  final Map<String, Service> _services = {};

  ServiceRegistry(FrameworkClient client) : super(client);

  Service get(String identifier) => _services[identifier];

  void register(Service service) {
    _services[service.name] = service;
  }

  Future<void> setupAll() =>
      Future.wait(_services.values.map((service) => service.setup(client)));
}
