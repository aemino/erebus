import 'dart:async';

import '../client.dart';

abstract class Service {
  String get name;

  FutureOr<void> setup(FrameworkClient client);
}
