import 'number.dart';
import 'string.dart';

// ignore: avoid_classes_with_only_static_members
abstract class Resolver {
  static final NumberResolver number = NumberResolver();
  static final IntegerResolver integer = IntegerResolver();
  static final StringResolver string = StringResolver();
}
