import 'dart:async';

import '../command/arguments.dart';
import '../command/structure.dart';

abstract class ResolverOptions<T> {
  final Resolver<T, ResolverOptions<T>> resolver;

  ResolverOptions(this.resolver);
}

abstract class Resolver<T, O extends ResolverOptions<T>> {
  ResolverOptions call();
  FutureOr<T> resolve(CommandContext context, ParsedArgument arg, O options);
}
