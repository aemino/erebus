import '../command/exceptions.dart';
import 'base.dart';

class _NumberResolverOptions<T extends num> extends ResolverOptions<T> {
  final T min;
  final T max;

  _NumberResolverOptions(_BaseNumberResolver<T> resolver, {this.min, this.max})
      : super(resolver);
}

abstract class _BaseNumberResolver<T extends num>
    extends Resolver<T, _NumberResolverOptions<T>> {
  _NumberResolverOptions call({T min, T max}) =>
      _NumberResolverOptions<T>(this, min: min, max: max);

  T parse(String arg);

  T resolve(context, arg, options) {
    final value = parse(arg.toString());

    if (value == null) throw UserInputException('resolvers.number.invalid');

    if (options.min != null && value < options.min)
      throw UserInputException('resolvers.number.minimum', {'value': value});

    if (options.max != null && value > options.max)
      throw UserInputException('resolvers.number.maximum', {'value': value});

    return value;
  }
}

class NumberResolver extends _BaseNumberResolver<num> {
  num parse(arg) => num.tryParse(arg);
}

class IntegerResolver extends _BaseNumberResolver<int> {
  int parse(arg) => int.tryParse(arg);
}
