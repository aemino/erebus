import '../command/exceptions.dart';
import 'base.dart';

class _StringResolverOptions extends ResolverOptions<String> {
  final int min;
  final int max;

  _StringResolverOptions(StringResolver resolver, {this.min, this.max})
      : super(resolver);
}

class StringResolver extends Resolver<String, _StringResolverOptions> {
  _StringResolverOptions call({int min, int max}) =>
      _StringResolverOptions(this, min: min, max: max);

  String resolve(context, arg, options) {
    final value = arg.toString();

    if (options.min != null && value.length < options.min)
      throw UserInputException(
          'resolvers.string.minimum', {'length': value.length});

    if (options.max != null && value.length > options.max)
      throw UserInputException(
          'resolvers.string.maximum', {'length': value.length});

    return value;
  }
}
